#include <Wire.h>
#include <RTClib.h>

// These are in 24-hour time, and comma-separated.
// For example, to run at 8AM, noon, 8PM, and 11PM,
// the line below would look like
// const int RUN_AT[]  = {8, 12, 20, 23};

// Run the sump at 11AM and 5PM
int RUN_AT[]  = {11, 17};

// Run Duration, in minutes
const int RUN_FOR   = 5;

////////////////////////////////////////////////////////
// No changes necessary below this line.

#define SET_CLOCK 0

RTC_DS3231 RTC;

const int SUMP_PIN  =  9;
const int LED_PIN   = 10;


void delaySeconds (int seconds) {
  for (int i = 0 ; i < seconds; i++) {
    delay (998);
  }
}
void delayMinutes (int minutes) {
  for (int i = 0 ; i < minutes ; i ++) {
    /*
    Serial.print("Slept for ");
    Serial.print(i);
    Serial.print(" minutes of ");
    Serial.print(minutes);
    Serial.println();
    */
    delaySeconds(60);
  }
}

void setAlarm (int hour, int minutes) {
  RTC.setAlarm1Simple(hour, minutes);
  RTC.turnOnAlarm(1);  
  Serial.print("Set alarm for ");
  Serial.print(hour);
  Serial.print(":");
  Serial.print(minutes);
  Serial.print(":00");
  Serial.println();
}

#define NUMREADINGS 10
int averageRead (int pin) {
  int readings[NUMREADINGS];
  for (int i = 0; i < NUMREADINGS; i++) {
    readings[i] = analogRead(pin);
    delay(100);
  }

  int sum = 0;
  for (int i = 0; i < NUMREADINGS; i++) {
    sum += readings[i];
  }

  return (sum / NUMREADINGS);
}

// Pins are A0, A1, and A2
// A0 is Alarm 1
// A1 is Alarm 2
// A2 is the number of minutes (0-10)
void setAlarmFromAnalog (int pin, int alarmIndex) {
  int alarm = map (averageRead(pin), 0, 1023, 0, 23);
  Serial.print("Setting alarm ");
  if (pin == A0) {
    Serial.print('1');  
  } else if (pin == A1) {
    Serial.print('2');
  }
  Serial.print(" to ");
  Serial.print(alarm);
  Serial.println();
  // Store it in the global alarm array.
  RUN_AT[alarmIndex] = alarm;
}

void setup () {
  Serial.begin(9600);

  pinMode(SUMP_PIN, OUTPUT);
  pinMode(LED_PIN, OUTPUT);
  
  Wire.begin();
  RTC.begin();

  if (SET_CLOCK) RTC.adjust(DateTime(__DATE__, __TIME__));
  
  DateTime now = RTC.now();
  // Set the alarm for the top of the next minute.
  int hour = now.hour();
  int minutes = (now.minute() + 1) % 60;
  setAlarm(hour, minutes);

  setAlarmFromAnalog(A0, 0);
  setAlarmFromAnalog(A1, 1);
  
  if (RTC.checkAlarmEnabled(1)) {
    Serial.println("Alarm 1 Enabled.");
  }
}

void printTime (DateTime now) {    
  Serial.print(now.year(), DEC);
  Serial.print('/');
  Serial.print(now.month(), DEC);
  Serial.print('/');
  Serial.print(now.day(), DEC);
  Serial.print(' ');
  Serial.print(now.hour(), DEC);
  Serial.print(':');
  Serial.print(now.minute(), DEC);
  Serial.print(':');
  Serial.print(now.second(), DEC);
  Serial.println();
}

void runSump () {
  Serial.println("\tRUNNING THE SUMP.");
  digitalWrite(SUMP_PIN, HIGH);
  digitalWrite(LED_PIN, HIGH);
  delayMinutes(RUN_FOR);
  digitalWrite(SUMP_PIN, LOW);
  digitalWrite(LED_PIN, LOW);
  Serial.println("\tDONE RUNNING THE SUMP.");
}

void loop() {  
  DateTime now = RTC.now();
  printTime(now);
  
  // The alarm should go off every hour, on the hour,
  // including when we wake up.
  if (RTC.checkIfAlarm(1)) {
    
    Serial.print("Alarm Triggered: ");
    printTime(now);

    // Check if this is an hour that we should run the sump.
    for (int i = 0 ; i < (sizeof(RUN_AT)/sizeof(*RUN_AT)); i++) {
      Serial.print("Checking if ");
      Serial.print(now.hour());
      Serial.print(" is ");
      Serial.print(RUN_AT[i]); 
      Serial.println();

      if (now.hour() == RUN_AT[i]) {
        Serial.println("\tIT IS THE APPOINTED HOUR.");
        // Run the sump for the defined number of minutes.
        runSump();
      }
    }
    
    // Set the alarm for the top of the next hour.
    int hour = (now.hour() + 1) % 24;
    int minutes = 0;
    setAlarm(hour, minutes);
  }

  // Wait for 1 minute.
  delayMinutes(1);
}

